ubuntu下stm32f407环境(正点原子)

## 1.交叉编译
```cpp

sudo apt install binutils-arm-none-eabi
sudo apt install gcc-arm-none-eabi
sudo apt install gdb-arm-none-eabi

```
如果没有gdb-arm-none-eabi 请看https://zhuanlan.zhihu.com/p/134031693

## 2.安装stlink
1.安装libusb
```cpp

sudo apt-get install libusb-dev
git clone https://github.com/texane/stlink.git 
cd stlink/ 
mkdir build 
cmake ..
make && sudo make install 

```

安装完成后

```cpp

xz@xiaqiu:~/study/stm32/stlink/build$ lsusb
Bus 001 Device 002: ID 8087:8001 Intel Corp. 
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 003 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 002 Device 006: ID 04f2:b502 Chicony Electronics Co., Ltd Integrated Camera
Bus 002 Device 005: ID 8087:07dc Intel Corp. 
Bus 002 Device 008: ID 0483:3748 STMicroelectronics ST-LINK/V2
Bus 002 Device 010: ID 18f8:0f99 [Maxxter] Optical gaming mouse
Bus 002 Device 012: ID 1c4f:0002 SiGma Micro Keyboard TRACER Gamma Ivory
Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
xz@xiaqiu:~/study/stm32/stlink/build$ 

```

配置选项
makefile.common
```cpp

TOP = $(shell pwd)
PROGRAM = stm32f4_out
LIBDIR = $(TOP)/lib
TypeOfMCU=STM32F407xx

TC=arm-none-eabi
CC=$(TC)-gcc
LD=$(TC)-ld -v
OBJCOPY=$(TC)-objcopy
AR=$(TC)-ar
GDB=$(TC)-gdb
INCLUDE=-I$(TOP)/include

COMMONFLAGS=-g -mcpu=cortex-m3 -mthumb
COMMONFLAGSlib=$(COMMONFLAGS)

CFLAGS+=$(COMMONFLAGS) -Wall -Werror $(INCLUDE)
CFLAGS+=-D $(TypeOfMCU)
CFLAGS+=-D VECT_TAB_FLASH

CFLAGSlib+=$(COMMONFLAGSlib) -Wall -Werror $(INCLUDE)
CFLAGSlib+=-D $(TypeOfMCU)
CFLAGSlib+=-D VECT_TAB_FLASH

```

## 标准例程-寄存器版本 - 实验1跑马灯实验
```cpp

xz@xiaqiu:~/study/stm32/ubuntu-stm32_1$ tree -L 3
.
├── build
├── hardware
│   ├── led
│   │   ├── led.c
│   │   ├── led.h
│   │   └── led.o
│   ├── libstm32.a
│   ├── makefile
│   └── system
│       ├── delay
│       └── sys
├── lib
│   ├── libapp.a
│   └── libstm32.a
├── makefile
├── makefile.common
├── STM32F407VGTx_FLASH.ld
├── stm32f4_out.bin
├── stm32f4_out.elf
├── stm32f4_out.hex
├── stm32f4_out.info_code
├── stm32f4_out.info_elf
├── stm32f4_out.info_size
├── stm32f4_out.info_symbol
├── tags
└── user
    ├── libapp.a
    ├── main.c
    ├── main.o
    ├── makefile
    ├── startup_stm32f40_41xxx.o
    └── startup_stm32f40_41xxx.s

8 directories, 24 files
xz@xiaqiu:~/study/stm32/ubuntu-stm32_1$ 

```
上层 makefile
` ubuntu-stm32_1/makefile `
```cpp

# general Makefile

include makefile.common
LDFLAGS=$(COMMONFLAGS) -fno-exceptions -ffunction-sections -fdata-sections -L$(LIBDIR) -nostartfiles -Wl,--gc-sections,-TSTM32F407VGTx_FLASH.ld
LDLIBS+=-lstm32
LDLIBS+=-lapp

all: user  hardware
    $(CC) -g -o $(PROGRAM).elf $(LDFLAGS) \
        -Wl,--whole-archive \
        user/libapp.a \
        -Wl,--no-whole-archive \
        $(LDLIBS)
    $(OBJCOPY) -O ihex $(PROGRAM).elf $(PROGRAM).hex
    $(OBJCOPY) -O binary $(PROGRAM).elf $(PROGRAM).bin
    #Extract info contained in ELF to readable text-files:
    arm-none-eabi-readelf -a $(PROGRAM).elf > $(PROGRAM).info_elf
    arm-none-eabi-size -d -B -t $(PROGRAM).elf > $(PROGRAM).info_size
    arm-none-eabi-objdump -S $(PROGRAM).elf > $(PROGRAM).info_code
    arm-none-eabi-nm -t d -S --size-sort -s $(PROGRAM).elf > $(PROGRAM).info_symbol

hardware:ECHO
    $(MAKE) -C $@
user:ECHO
    $(MAKE) -C $@
ECHO:
    @echo $@
.PHONY: clean

# 总控的makefile使用$(MAKE)这个宏调用，子目录下的makefile
# 这里的意思是先进入-C之后的目录中然后执行该目录下的makefile

clean:hardware_clean user_clean
    rm $(PROGRAM).* 
hardware_clean:
    @cd hardware && make clean
user_clean:
    @cd user && make clean


```

` ubuntu-stm32_1/user/makefile `

```cpp

include ../makefile.common

CFLAGSlib+=-c
USER = $(shell pwd)

src += ./*.c
src += ./*.s
obj = ./*.o 
.PHONY :obj

libapp.a : $(obj)
    $(AR) cr $@ \
    $^
    if [ -e libapp.a ]; then cp -f libapp.a ../lib; fi

$(obj):
    $(CC)  $(CFLAGSlib) \
    -I../include \
    -I../hardware/system/delay \
    -I../hardware/system/sys \
    -I../hardware/led \
    $(src)
    

.PHONY :clean
clean :
    rm libapp.a  $(obj)

```

ubuntu-stm32_1/hardware/makefile
```cpp

# libs Makefile
include ../makefile.common
CFLAGSlib+=-c

STM32LIB = $(shell pwd)

obj1 = $(STM32LIB)/system/sys/*.o
obj2 = $(STM32LIB)/system/delay/*.o
obj3 = $(STM32LIB)/led/*.o
inc1 = $(STM32LIB)/led
inc2 = $(STM32LIB)/system/delay
inc3 = $(STM32LIB)/system/sys

libstm32.a: $(obj3) $(obj2) $(obj1)
    echo "comlile stm32lib.a"
    $(AR) cr $(STM32LIB)/$@ $^
    echo "make stm32lib success"
    if [ -e libstm32.a ]; then cp -f libstm32.a ../lib; fi


$(obj1) :
    echo "comlile obj1"
    cd $(STM32LIB)/system/sys && \
    $(CC) $(CFLAGSlib) \
        -I$(inc1) -I$(inc2) -I$(inc3) \
        *.c

$(obj2) :
    echo "comlile obj2"
    cd $(STM32LIB)/system/delay && \
    $(CC) $(CFLAGSlib) \
        -I$(inc1) -I$(inc2) -I$(inc3) \
        -I../../../../include \
        *.c

$(obj3) :
    echo "comlile obj3"
    cd $(STM32LIB)/led && \
    $(CC) $(CFLAGSlib) \
        -I$(inc1) -I$(inc2) -I$(inc3) \
        -I../../../../include \
        *.c


.PHONY: clean 
clean:
    rm -f $(STM32LIB)/libstm32.a $(obj1) $(obj2) $(obj3)


```
编译

```cpp

xz@xiaqiu:~/study/stm32/ubuntu-stm32_1$ make clean
make[1]: 进入目录“/home/xz/study/stm32/ubuntu-stm32_1/hardware”
rm -f /home/xz/study/stm32/ubuntu-stm32_1/hardware/libstm32.a /home/xz/study/stm32/ubuntu-stm32_1/hardware/system/sys/*.o /home/xz/study/stm32/ubuntu-stm32_1/hardware/system/delay/*.o /home/xz/study/stm32/ubuntu-stm32_1/hardware/led/*.o
make[1]: 离开目录“/home/xz/study/stm32/ubuntu-stm32_1/hardware”
make[1]: 进入目录“/home/xz/study/stm32/ubuntu-stm32_1/user”
rm libapp.a  ./*.o 
make[1]: 离开目录“/home/xz/study/stm32/ubuntu-stm32_1/user”
rm stm32f4_out.* 
xz@xiaqiu:~/study/stm32/ubuntu-stm32_1$ make
ECHO
make -C user
make[1]: 进入目录“/home/xz/study/stm32/ubuntu-stm32_1/user”
arm-none-eabi-gcc  -g -mcpu=cortex-m3 -mthumb -Wall -Werror -I/home/xz/study/stm32/ubuntu-stm32_1/user/include -D STM32F407xx -D VECT_TAB_FLASH -c \
-I../include \
-I../hardware/system/delay \
-I../hardware/system/sys \
-I../hardware/led \
./*.c ./*.s
arm-none-eabi-ar cr libapp.a \
*.o
if [ -e libapp.a ]; then cp -f libapp.a ../lib; fi
make[1]: 离开目录“/home/xz/study/stm32/ubuntu-stm32_1/user”
make -C hardware
make[1]: 进入目录“/home/xz/study/stm32/ubuntu-stm32_1/hardware”
echo "comlile obj3"
comlile obj3
cd /home/xz/study/stm32/ubuntu-stm32_1/hardware/led && \
arm-none-eabi-gcc -g -mcpu=cortex-m3 -mthumb -Wall -Werror -I/home/xz/study/stm32/ubuntu-stm32_1/hardware/include -D STM32F407xx -D VECT_TAB_FLASH -c \
    -I/home/xz/study/stm32/ubuntu-stm32_1/hardware/led -I/home/xz/study/stm32/ubuntu-stm32_1/hardware/system/delay -I/home/xz/study/stm32/ubuntu-stm32_1/hardware/system/sys \
    -I../../../../include \
    *.c
echo "comlile obj2"
comlile obj2
cd /home/xz/study/stm32/ubuntu-stm32_1/hardware/system/delay && \
arm-none-eabi-gcc -g -mcpu=cortex-m3 -mthumb -Wall -Werror -I/home/xz/study/stm32/ubuntu-stm32_1/hardware/include -D STM32F407xx -D VECT_TAB_FLASH -c \
    -I/home/xz/study/stm32/ubuntu-stm32_1/hardware/led -I/home/xz/study/stm32/ubuntu-stm32_1/hardware/system/delay -I/home/xz/study/stm32/ubuntu-stm32_1/hardware/system/sys \
    -I../../../../include \
    *.c
echo "comlile obj1"
comlile obj1
cd /home/xz/study/stm32/ubuntu-stm32_1/hardware/system/sys && \
arm-none-eabi-gcc -g -mcpu=cortex-m3 -mthumb -Wall -Werror -I/home/xz/study/stm32/ubuntu-stm32_1/hardware/include -D STM32F407xx -D VECT_TAB_FLASH -c \
    -I/home/xz/study/stm32/ubuntu-stm32_1/hardware/led -I/home/xz/study/stm32/ubuntu-stm32_1/hardware/system/delay -I/home/xz/study/stm32/ubuntu-stm32_1/hardware/system/sys \
    *.c
echo "comlile stm32lib.a"
comlile stm32lib.a
arm-none-eabi-ar cr /home/xz/study/stm32/ubuntu-stm32_1/hardware/libstm32.a /home/xz/study/stm32/ubuntu-stm32_1/hardware/led/*.o /home/xz/study/stm32/ubuntu-stm32_1/hardware/system/delay/*.o /home/xz/study/stm32/ubuntu-stm32_1/hardware/system/sys/*.o
echo "make stm32lib success"
make stm32lib success
if [ -e libstm32.a ]; then cp -f libstm32.a ../lib; fi
make[1]: 离开目录“/home/xz/study/stm32/ubuntu-stm32_1/hardware”
arm-none-eabi-gcc -g -o stm32f4_out.elf -g -mcpu=cortex-m3 -mthumb -fno-exceptions -ffunction-sections -fdata-sections -L/home/xz/study/stm32/ubuntu-stm32_1/lib -nostartfiles -Wl,--gc-sections,-TSTM32F407VGTx_FLASH.ld \
    -Wl,--whole-archive \
    user/libapp.a \
    -Wl,--no-whole-archive \
    -lstm32 -lapp
arm-none-eabi-objcopy -O ihex stm32f4_out.elf stm32f4_out.hex
arm-none-eabi-objcopy -O binary stm32f4_out.elf stm32f4_out.bin
#Extract info contained in ELF to readable text-files:
arm-none-eabi-readelf -a stm32f4_out.elf > stm32f4_out.info_elf
arm-none-eabi-size -d -B -t stm32f4_out.elf > stm32f4_out.info_size
arm-none-eabi-objdump -S stm32f4_out.elf > stm32f4_out.info_code
arm-none-eabi-nm -t d -S --size-sort -s stm32f4_out.elf > stm32f4_out.info_symbol
xz@xiaqiu:~/study/stm32/ubuntu-stm32_1$ 
xz@xiaqiu:~/study/stm32/ubuntu-stm32_1$ ls
build     makefile.common         stm32f4_out.hex        stm32f4_out.info_symbol
hardware  STM32F407VGTx_FLASH.ld  stm32f4_out.info_code  tags
lib       stm32f4_out.bin         stm32f4_out.info_elf   user
makefile  stm32f4_out.elf         stm32f4_out.info_size
xz@xiaqiu:~/study/stm32/ubuntu-stm32_1$ 

```
## 标准例程-库函数版本 - 实验1跑马灯实验

```cpp

xz@xiaqiu:~/study/stm32/ubuntu-stm32_2$ tree -L 3
.
├── build
├── hardware
│   ├── core
│   │   ├── core_cm4.h
│   │   ├── core_cm4_simd.h
│   │   ├── core_cmFunc.h
│   │   ├── core_cmInstr.h
│   │   ├── startup_stm32f40_41xxx.o
│   │   └── startup_stm32f40_41xxx.s
│   ├── fwlib
│   │   ├── inc
│   │   └── src
│   ├── led
│   │   ├── led.c
│   │   ├── led.h
│   │   └── led.o
│   ├── libstm32.a
│   ├── makefile
│   └── system
│       ├── delay
│       ├── sys
│       └── usart
├── lib
│   ├── libapp.a
│   └── libstm32.a
├── makefile
├── makefile.common
├── STM32F407VGTx_FLASH.ld
├── stm32f4_out.bin
├── stm32f4_out.elf
├── stm32f4_out.hex
├── stm32f4_out.info_code
├── stm32f4_out.info_elf
├── stm32f4_out.info_size
├── stm32f4_out.info_symbol
├── tags
└── user
    ├── libapp.a
    ├── main.c
    ├── main.o
    ├── makefile
    ├── readme.md
    ├── stm32f4xx_conf.h
    ├── stm32f4xx.h
    ├── stm32f4xx_it.c
    ├── stm32f4xx_it.h
    ├── stm32f4xx_it.o
    ├── system_stm32f4xx.c
    ├── system_stm32f4xx.h
    └── system_stm32f4xx.o

13 directories, 37 files
xz@xiaqiu:~/study/stm32/ubuntu-stm32_2$ 


```

ubuntu-stm32_2/makefile
```cpp

# general Makefile

include makefile.common
LDFLAGS=$(COMMONFLAGS) -fno-exceptions -ffunction-sections -fdata-sections -L$(LIBDIR) -nostartfiles -Wl,--gc-sections,-TSTM32F407VGTx_FLASH.ld
LDLIBS+=-lstm32
LDLIBS+=-lapp

all: user  hardware
    $(CC) -g -o $(PROGRAM).elf $(LDFLAGS) \
        -Wl,--whole-archive \
        user/libapp.a \
        -Wl,--no-whole-archive \
        $(LDLIBS)
    $(OBJCOPY) -O ihex $(PROGRAM).elf $(PROGRAM).hex
    $(OBJCOPY) -O binary $(PROGRAM).elf $(PROGRAM).bin
    #Extract info contained in ELF to readable text-files:
    arm-none-eabi-readelf -a $(PROGRAM).elf > $(PROGRAM).info_elf
    arm-none-eabi-size -d -B -t $(PROGRAM).elf > $(PROGRAM).info_size
    arm-none-eabi-objdump -S $(PROGRAM).elf > $(PROGRAM).info_code
    arm-none-eabi-nm -t d -S --size-sort -s $(PROGRAM).elf > $(PROGRAM).info_symbol

hardware:ECHO
    $(MAKE) -C  $@
user:ECHO
    $(MAKE) -C  $@
ECHO:
    echo $@
.PHONY: clean

# 总控的makefile使用$(MAKE)这个宏调用，子目录下的makefile
# 这里的意思是先进入-C之后的目录中然后执行该目录下的makefile


clean:hardware_clean user_clean
    rm $(PROGRAM).* 
hardware_clean:
    @cd hardware && make clean
user_clean:
    @cd user && make clean

```

ubuntu-stm32_2/user/makefile
```cpp

include ../makefile.common

CFLAGSlib+=-c -lc
USER = $(shell pwd)

src += ./*.c
obj = ./*.o 
.PHONY :obj

libapp.a : $(obj)
    $(AR) cr $@ \
    $^
    if [ -e libapp.a ]; then cp -f libapp.a ../lib; fi

$(obj):
    $(CC)  $(CFLAGSlib) \
    -I./ \
    -I../include \
    -I../hardware/core \
    -I../hardware/fwlib/inc \
    -I../hardware/system/delay \
    -I../hardware/system/sys \
    -I../hardware/system/usart \
    -I../hardware/led \
    $(src)
    

.PHONY :clean
clean :
    rm libapp.a  $(obj)

```
ubuntu-stm32_2/hardware/makefile
```cpp

# libs Makefile
include ../makefile.common
CFLAGSlib+=-c -Wall -Wno-unused-but-set-variable

STM32LIB = $(shell pwd)

obj1 = $(STM32LIB)/core/*.o
obj2 = $(STM32LIB)/system/delay/*.o
obj3 = $(STM32LIB)/system/sys/*.o
obj4 = $(STM32LIB)/system/usart/*.o
obj5 = $(STM32LIB)/led/*.o
obj6 = $(STM32LIB)/fwlib/src/*.o

inc1 = $(STM32LIB)/core
inc2 = $(STM32LIB)/system/delay
inc3 = $(STM32LIB)/system/sys
inc4 = $(STM32LIB)/system/usart
inc5 = $(STM32LIB)/led/
inc6 = $(STM32LIB)/fwlib/inc
inc7 = $(STM32LIB)/../user

libstm32.a: $(obj1) $(obj2) $(obj3) $(obj4) $(obj5) $(obj6)
    echo "comlile stm32lib.a"
    $(AR) cr $(STM32LIB)/$@ $^
    echo "make stm32lib success"
    if [ -e libstm32.a ]; then cp -f libstm32.a ../lib; fi
    

$(obj1) :
    echo "comlile obj1"
    cd $(STM32LIB)/core && \
    $(CC) $(CFLAGSlib) \
        -I$(inc1) -I$(inc2) -I$(inc3) -I$(inc4) -I$(inc5) -I$(inc6) -I$(inc7) \
        *.s
    
$(obj2) :
    echo "comlile obj2"
    cd $(STM32LIB)/system/delay && \
    $(CC) $(CFLAGSlib) \
        -I$(inc1) -I$(inc2) -I$(inc3) -I$(inc4) -I$(inc5) -I$(inc6) -I$(inc7) \
        *.c

$(obj3) :
    echo "comlile obj3"
    cd $(STM32LIB)/system/sys && \
    $(CC) $(CFLAGSlib) \
        -I$(inc1) -I$(inc2) -I$(inc3) -I$(inc4) -I$(inc5) -I$(inc6) -I$(inc7) \
        *.c

$(obj4) :
    echo "comlile obj4"
    cd $(STM32LIB)/system/usart && \
    $(CC) $(CFLAGSlib) \
        -I$(inc1) -I$(inc2) -I$(inc3) -I$(inc4) -I$(inc5) -I$(inc6) -I$(inc7) \
        *.c

$(obj5) :
    echo "comlile obj5"
    cd $(STM32LIB)/led && \
    $(CC) $(CFLAGSlib) \
        -I$(inc1) -I$(inc2) -I$(inc3) -I$(inc4) -I$(inc5) -I$(inc6) -I$(inc7) \
        *.c

$(obj6) :
    echo "comlile obj6"
    cd $(STM32LIB)/fwlib/src && \
    $(CC) $(CFLAGSlib) \
        -I$(inc1) -I$(inc2) -I$(inc3) -I$(inc4) -I$(inc5) -I$(inc6) -I$(inc7) \
        *.c

.PHONY: clean 
clean:
    rm -f $(STM32LIB)/libstm32.a $(obj1) $(obj2) $(obj3) $(obj4) $(obj5) $(obj6)


```
编译
```cpp

xz@xiaqiu:~/study/stm32/ubuntu-stm32_2$ make 
echo ECHO
ECHO
make -C  user
make[1]: 进入目录“/home/xz/study/stm32/ubuntu-stm32_2/user”
arm-none-eabi-gcc  -g -mcpu=cortex-m3 -mthumb -Wall -Werror -I/home/xz/study/stm32/ubuntu-stm32_2/user/include -D STM32F407xx  -D VECT_TAB_FLASH -c -lc \
-I./ \
-I../include \
-I../hardware/core \
-I../hardware/fwlib/inc \
-I../hardware/system/delay \
-I../hardware/system/sys \
-I../hardware/system/usart \
-I../hardware/led \
./*.c
arm-none-eabi-ar cr libapp.a \
*.o
if [ -e libapp.a ]; then cp -f libapp.a ../lib; fi
make[1]: 离开目录“/home/xz/study/stm32/ubuntu-stm32_2/user”
make -C  hardware
make[1]: 进入目录“/home/xz/study/stm32/ubuntu-stm32_2/hardware”
echo "comlile obj1"
comlile obj1
cd /home/xz/study/stm32/ubuntu-stm32_2/hardware/core && \
arm-none-eabi-gcc -g -mcpu=cortex-m3 -mthumb -Wall -Werror -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/include -D STM32F407xx  -D VECT_TAB_FLASH -c -Wall -Wno-unused-but-set-variable \
    -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/core -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/system/delay -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/system/sys -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/system/usart -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/led/ -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/fwlib/inc -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/../user \
    *.s
echo "comlile obj2"
comlile obj2
cd /home/xz/study/stm32/ubuntu-stm32_2/hardware/system/delay && \
arm-none-eabi-gcc -g -mcpu=cortex-m3 -mthumb -Wall -Werror -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/include -D STM32F407xx  -D VECT_TAB_FLASH -c -Wall -Wno-unused-but-set-variable \
    -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/core -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/system/delay -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/system/sys -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/system/usart -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/led/ -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/fwlib/inc -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/../user \
    *.c
echo "comlile obj3"
comlile obj3
cd /home/xz/study/stm32/ubuntu-stm32_2/hardware/system/sys && \
arm-none-eabi-gcc -g -mcpu=cortex-m3 -mthumb -Wall -Werror -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/include -D STM32F407xx  -D VECT_TAB_FLASH -c -Wall -Wno-unused-but-set-variable \
    -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/core -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/system/delay -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/system/sys -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/system/usart -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/led/ -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/fwlib/inc -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/../user \
    *.c
echo "comlile obj4"
comlile obj4
cd /home/xz/study/stm32/ubuntu-stm32_2/hardware/system/usart && \
arm-none-eabi-gcc -g -mcpu=cortex-m3 -mthumb -Wall -Werror -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/include -D STM32F407xx  -D VECT_TAB_FLASH -c -Wall -Wno-unused-but-set-variable \
    -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/core -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/system/delay -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/system/sys -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/system/usart -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/led/ -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/fwlib/inc -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/../user \
    *.c
echo "comlile obj5"
comlile obj5
cd /home/xz/study/stm32/ubuntu-stm32_2/hardware/led && \
arm-none-eabi-gcc -g -mcpu=cortex-m3 -mthumb -Wall -Werror -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/include -D STM32F407xx  -D VECT_TAB_FLASH -c -Wall -Wno-unused-but-set-variable \
    -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/core -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/system/delay -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/system/sys -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/system/usart -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/led/ -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/fwlib/inc -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/../user \
    *.c
echo "comlile obj6"
comlile obj6
cd /home/xz/study/stm32/ubuntu-stm32_2/hardware/fwlib/src && \
arm-none-eabi-gcc -g -mcpu=cortex-m3 -mthumb -Wall -Werror -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/include -D STM32F407xx  -D VECT_TAB_FLASH -c -Wall -Wno-unused-but-set-variable \
    -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/core -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/system/delay -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/system/sys -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/system/usart -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/led/ -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/fwlib/inc -I/home/xz/study/stm32/ubuntu-stm32_2/hardware/../user \
    *.c
echo "comlile stm32lib.a"
comlile stm32lib.a
arm-none-eabi-ar cr /home/xz/study/stm32/ubuntu-stm32_2/hardware/libstm32.a /home/xz/study/stm32/ubuntu-stm32_2/hardware/core/*.o /home/xz/study/stm32/ubuntu-stm32_2/hardware/system/delay/*.o /home/xz/study/stm32/ubuntu-stm32_2/hardware/system/sys/*.o /home/xz/study/stm32/ubuntu-stm32_2/hardware/system/usart/*.o /home/xz/study/stm32/ubuntu-stm32_2/hardware/led/*.o /home/xz/study/stm32/ubuntu-stm32_2/hardware/fwlib/src/*.o
echo "make stm32lib success"
make stm32lib success
if [ -e libstm32.a ]; then cp -f libstm32.a ../lib; fi
make[1]: 离开目录“/home/xz/study/stm32/ubuntu-stm32_2/hardware”
arm-none-eabi-gcc -g -o stm32f4_out.elf -g -mcpu=cortex-m3 -mthumb -fno-exceptions -ffunction-sections -fdata-sections -L/home/xz/study/stm32/ubuntu-stm32_2/lib -nostartfiles -Wl,--gc-sections,-TSTM32F407VGTx_FLASH.ld \
    -Wl,--whole-archive \
    user/libapp.a \
    -Wl,--no-whole-archive \
    -lstm32 -lapp
arm-none-eabi-objcopy -O ihex stm32f4_out.elf stm32f4_out.hex
arm-none-eabi-objcopy -O binary stm32f4_out.elf stm32f4_out.bin
#Extract info contained in ELF to readable text-files:
arm-none-eabi-readelf -a stm32f4_out.elf > stm32f4_out.info_elf
arm-none-eabi-size -d -B -t stm32f4_out.elf > stm32f4_out.info_size
arm-none-eabi-objdump -S stm32f4_out.elf > stm32f4_out.info_code
arm-none-eabi-nm -t d -S --size-sort -s stm32f4_out.elf > stm32f4_out.info_symbol
xz@xiaqiu:~/study/stm32/ubuntu-stm32_2$ 
xz@xiaqiu:~/study/stm32/ubuntu-stm32_2$ ls
build     makefile.common         stm32f4_out.hex        stm32f4_out.info_symbol
hardware  STM32F407VGTx_FLASH.ld  stm32f4_out.info_code  tags
lib       stm32f4_out.bin         stm32f4_out.info_elf   user
makefile  stm32f4_out.elf         stm32f4_out.info_size
xz@xiaqiu:~/study/stm32/ubuntu-stm32_2$ 

```

## 标准例程-HAL库版本 - 实验1跑马灯实验
```cpp

xz@xiaqiu:~/study/stm32/ubuntu-stm32_3$ tree -L 3
.
├── build
├── hardware
│   ├── core
│   │   ├── cmsis_armcc.h
│   │   ├── cmsis_armclang.h
│   │   ├── cmsis_compiler.h
│   │   ├── cmsis_gcc.h
│   │   ├── cmsis_version.h
│   │   ├── core_cm4.h
│   │   ├── core_cmFunc.h
│   │   ├── core_cmInstr.h
│   │   ├── core_cmSimd.h
│   │   ├── mpu_armv7.h
│   │   ├── startup_stm32f40_41xxx.o
│   │   └── startup_stm32f40_41xxx.s
│   ├── led
│   │   ├── led.c
│   │   ├── led.h
│   │   └── led.o
│   ├── libstm32.a
│   ├── Makefile
│   ├── startup_stm32f407xx.s
│   ├── STM32F4xx_HAL_Driver
│   │   ├── Inc
│   │   └── Src
│   └── system
│       ├── delay
│       ├── sys
│       └── usart
├── include
│   ├── stm32f4xx_hal_conf.h
│   └── stm32f4xx_hal.h
├── lib
│   ├── libapp.a
│   └── libstm32.a
├── makefile
├── makefile.common
├── STM32F407VGTx_FLASH.ld
├── stm32f4_out.bin
├── stm32f4_out.elf
├── stm32f4_out.hex
├── stm32f4_out.info_code
├── stm32f4_out.info_elf
├── stm32f4_out.info_size
├── stm32f4_out.info_symbol
├── tags
└── user
    ├── libapp.a
    ├── main.c
    ├── main.h
    ├── main.o
    ├── makefile
    ├── stm32f407xx.h
    ├── stm32f4xx.h
    ├── stm32f4xx_hal_conf.h
    ├── stm32f4xx_hal_msp.c
    ├── stm32f4xx_hal_msp.o
    ├── stm32f4xx_it.c
    ├── stm32f4xx_it.h
    ├── stm32f4xx_it.o
    ├── system_stm32f4xx.c
    ├── system_stm32f4xx.h
    └── system_stm32f4xx.o

14 directories, 49 files
xz@xiaqiu:~/study/stm32/ubuntu-stm32_3$ 

```

ubuntu-stm32_3/makefile
```cpp

# general Makefile

include makefile.common
LDFLAGS=$(COMMONFLAGS) -fno-exceptions -ffunction-sections -fdata-sections -L$(LIBDIR) -nostartfiles -Wl,--gc-sections,-TSTM32F407VGTx_FLASH.ld
LDLIBS+=-lstm32
LDLIBS+=-lapp

all: user  hardware
    $(CC) -g -o $(PROGRAM).elf $(LDFLAGS) \
        -Wl,--whole-archive \
        user/libapp.a \
        -Wl,--no-whole-archive \
        $(LDLIBS)
    $(OBJCOPY) -O ihex $(PROGRAM).elf $(PROGRAM).hex
    $(OBJCOPY) -O binary $(PROGRAM).elf $(PROGRAM).bin
    #Extract info contained in ELF to readable text-files:
    arm-none-eabi-readelf -a $(PROGRAM).elf > $(PROGRAM).info_elf
    arm-none-eabi-size -d -B -t $(PROGRAM).elf > $(PROGRAM).info_size
    arm-none-eabi-objdump -S $(PROGRAM).elf > $(PROGRAM).info_code
    arm-none-eabi-nm -t d -S --size-sort -s $(PROGRAM).elf > $(PROGRAM).info_symbol

hardware:ECHO
    $(MAKE) -C  $@
user:ECHO
    $(MAKE) -C  $@
ECHO:
    echo $@
.PHONY: clean

# 总控的makefile使用$(MAKE)这个宏调用，子目录下的makefile
# 这里的意思是先进入-C之后的目录中然后执行该目录下的makefile


clean:hardware_clean user_clean
    rm $(PROGRAM).* lib/*
hardware_clean:
    @cd hardware && make clean
user_clean:
    @cd user && make clean

```

ubuntu-stm32_3/user/makefile
```cpp

include ../makefile.common

CFLAGSlib+=-c
USER = $(shell pwd)

src += ./*.c
obj = ./*.o 
.PHONY :obj

libapp.a : $(obj)
    $(AR) cr $@ \
    $^
    @if [ -e libapp.a ]; then echo "cp -f libapp.a" && cp -f libapp.a ../lib; fi

$(obj):
    $(CC)  $(CFLAGSlib) \
    -I./ \
    -I../include \
    -I../hardware/STM32F4xx_HAL_Driver/Inc \
    -I../hardware/core \
    -I../hardware/system/sys \
    -I../hardware/system/delay \
    -I../hardware/system/usart \
    -I../hardware/led \
    $(src) 
.PHONY :clean
clean :
    rm libapp.a  $(obj)

```
ubuntu-stm32_3/hardware/makefile
```cpp

# libs Makefile
include ../makefile.common
CFLAGSlib+=-c -Wno-unused-but-set-variable

STM32LIB = $(shell pwd)

obj1 = $(STM32LIB)/core/*.o
obj2 = $(STM32LIB)/led/*.o
obj3 = $(STM32LIB)/STM32F4xx_HAL_Driver/Src/*.o
obj4 = $(STM32LIB)/system/sys/*.o
obj5 = $(STM32LIB)/system/usart/*.o
obj6 = $(STM32LIB)/system/delay/*.o

inc1 = $(STM32LIB)/core
inc2 = $(STM32LIB)/led
inc3 = $(STM32LIB)/STM32F4xx_HAL_Driver/Inc
inc4 = $(STM32LIB)/system/sys
inc5 = $(STM32LIB)/system/usart
inc6 = $(STM32LIB)/system/delay
inc7 = $(STM32LIB)/../user

libstm32.a: $(obj1) $(obj2) $(obj3) $(obj4) $(obj5) $(obj6)
    @echo "comlile libstm32.a"
    @$(AR) cr $(STM32LIB)/$@ $^
    @echo "make stm32lib success"
    @if [ -e $(STM32LIB)/libstm32.a ]; then cp -f $(STM32LIB)/libstm32.a ../lib; fi

$(obj1) :
    echo "comlile obj1"
    cd $(STM32LIB)/core/ && \
    $(CC) $(CFLAGSlib) \
    -I$(inc1) -I$(inc2) -I$(inc3) -I$(inc4) -I$(inc5) -I$(inc6) -I$(inc7) \
    *.s

$(obj2) :
    echo "comlile obj2"
    cd $(STM32LIB)/led/ && \
    $(CC) $(CFLAGSlib) \
        -I$(inc1) -I$(inc2) -I$(inc3) -I$(inc4) -I$(inc5) -I$(inc6) -I$(inc7)\
        -I../../../../include \
        *.c

$(obj3) :
    echo "comlile obj3"
    cd $(STM32LIB)/STM32F4xx_HAL_Driver/Src/ && \
    $(CC) $(CFLAGSlib) \
        -I$(inc1) -I$(inc2) -I$(inc3) -I$(inc4) -I$(inc5) -I$(inc6) -I$(inc7)\
        -I../../../../include \
        *.c

$(obj4) :
    echo "comlile obj4"
    cd $(STM32LIB)/system/sys/ && \
    $(CC) $(CFLAGSlib) \
        -I$(inc1) -I$(inc2) -I$(inc3) -I$(inc4) -I$(inc5) -I$(inc6) -I$(inc7)\
        -I../../../../include \
        *.c

$(obj5) :
    echo "comlile obj5"
    cd $(STM32LIB)/system/usart/ && \
    $(CC) $(CFLAGSlib) \
        -I$(inc1) -I$(inc2) -I$(inc3) -I$(inc4) -I$(inc5) -I$(inc6) -I$(inc7)\
        -I../../../../include \
        *.c

$(obj6) :
    echo "comlile obj6"
    cd $(STM32LIB)/system/delay/ && \
    $(CC) $(CFLAGSlib) \
        -I$(inc1) -I$(inc2) -I$(inc3) -I$(inc4) -I$(inc5) -I$(inc6) -I$(inc7)\
        -I../../../../include \
        *.c


.PHONY: clean 
clean:
    rm -f $(STM32LIB)/libstm32.a $(obj1) $(obj2) $(obj3) $(obj4) $(obj5) $(obj6)


```

## gdb 调试
连接stlink

```cpp

xz@xiaqiu:~/study/stm32/ubuntu-stm32_3/hardware$ lsusb
Bus 001 Device 002: ID 8087:8001 Intel Corp. 
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 003 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 002 Device 004: ID 04f2:b502 Chicony Electronics Co., Ltd Integrated Camera
Bus 002 Device 003: ID 8087:07dc Intel Corp. 
Bus 002 Device 009: ID 0483:3748 STMicroelectronics ST-LINK/V2
Bus 002 Device 007: ID 18f8:0f99 [Maxxter] Optical gaming mouse
Bus 002 Device 008: ID 1c4f:0002 SiGma Micro Keyboard TRACER Gamma Ivory
Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
xz@xiaqiu:~/study/stm32/ubuntu-stm32_3/hardware$ 

```

### 1运行openocd命令连接stlink

```cpp

openocd -f interface/stlink-v2.cfg -f target/stm32f4x_stlink.cfg

```

```cpp

xz@xiaqiu:~/study/stm32/ubuntu-stm32_3/hardware$ openocd -f interface/stlink-v2.cfg -f target/stm32f4x_stlink.cfg
Open On-Chip Debugger 0.10.0
Licensed under GNU GPL v2
For bug reports, read
    http://openocd.org/doc/doxygen/bugs.html
WARNING: target/stm32f4x_stlink.cfg is deprecated, please switch to target/stm32f4x.cfg
Info : auto-selecting first available session transport "hla_swd". To override use 'transport select <transport>'.
Info : The selected transport took over low-level target control. The results might differ compared to plain JTAG/SWD
adapter speed: 2000 kHz
adapter_nsrst_delay: 100
none separate
Error: couldn't bind tcl to socket: Address already in use
xz@xiaqiu:~/study/stm32/ubuntu-stm32_3/hardware$ 

```

端口被占用查看端口
```cpp

xz@xiaqiu:~/study/stm32/ubuntu-stm32_3/hardware$ sudo netstat -nap
[sudo] xz 的密码： 
激活Internet连接 (服务器和已建立连接的)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      796/cupsd           
tcp        0      0 0.0.0.0:6665            0.0.0.0:*               LISTEN      1211/crtmpserver    
tcp        0      0 0.0.0.0:6666            0.0.0.0:*               LISTEN      1211/crtmpserver    
tcp        0      0 0.0.0.0:9999            0.0.0.0:*               LISTEN      1211/crtmpserver    
tcp        0      0 0.0.0.0:1935            0.0.0.0:*               LISTEN      1211/crtmpserver    
tcp        0      0 0.0.0.0:8080            0.0.0.0:*               LISTEN      1211/crtmpserver    
tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      731/systemd-resolve 
... ...
xz@xiaqiu:~/study/stm32/ubuntu-stm32_3/hardware$ sudo pkill crtmpserver 
xz@xiaqiu:~/study/stm32/ubuntu-stm32_3/hardware$ 

```

pkill 占用进程后重新运行

```cpp

xz@xiaqiu:~/study/stm32/ubuntu-stm32_3/hardware$ openocd -f interface/stlink-v2.cfg -f target/stm32f4x_stlink.cfg
Open On-Chip Debugger 0.10.0
Licensed under GNU GPL v2
For bug reports, read
    http://openocd.org/doc/doxygen/bugs.html
WARNING: target/stm32f4x_stlink.cfg is deprecated, please switch to target/stm32f4x.cfg
Info : auto-selecting first available session transport "hla_swd". To override use 'transport select <transport>'.
Info : The selected transport took over low-level target control. The results might differ compared to plain JTAG/SWD
adapter speed: 2000 kHz
adapter_nsrst_delay: 100
none separate
Info : Unable to match requested speed 2000 kHz, using 1800 kHz
Info : Unable to match requested speed 2000 kHz, using 1800 kHz
Info : clock speed 1800 kHz
Info : STLINK v2 JTAG v35 API v2 SWIM v7 VID 0x0483 PID 0x3748
Info : using stlink api v2
Info : Target voltage: 3.237945
Info : stm32f4x.cpu: hardware has 6 breakpoints, 4 watchpoints
...

```

### 2.新终端输入命令下载程序
#### 输入命令

```cpp

telnet localhost 4444 

```

```cpp

xz@xiaqiu:~/study/stm32/ubuntu-stm32_3/hardware$ telnet localhost 4444 
Trying ::1...
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
Open On-Chip Debugger
> 

```

#### 下载程序

```cpp

reset halt   
flash probe 0    
stm32f4x mass_erase 0
flash write_bank 0 /home/xz/study/stm32/ubuntu-stm32_3/stm32f4_out.elf 0
reset run 

```

```cpp

xz@xiaqiu:~/study/stm32/ubuntu-stm32_3/hardware$ telnet localhost 4444 
Trying ::1...
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
Open On-Chip Debugger
> reset halt   
Unable to match requested speed 2000 kHz, using 1800 kHz
Unable to match requested speed 2000 kHz, using 1800 kHz
adapter speed: 1800 kHz
target halted due to debug-request, current mode: Thread 
xPSR: 0x01000000 pc: 0x08002f80 msp: 0x2001fffc
> flash probe 0    
device id = 0x10076413
flash size = 1024kbytes
flash 'stm32f2x' found at 0x08000000
> stm32f4x mass_erase 0
stm32x mass erase complete
> flash write_bank 0 /home/xz/study/stm32/ubuntu-stm32_3/stm32f4_out.elf 0
target halted due to breakpoint, current mode: Thread 
xPSR: 0x61000000 pc: 0x20000046 msp: 0x2001fffc
wrote 201112 bytes from file /home/xz/study/stm32/ubuntu-stm32_3/stm32f4_out.elf to flash bank 0 at offset 0x00000000 in 2.252120s (87.206 KiB/s)
> reset run 
Unable to match requested speed 2000 kHz, using 1800 kHz
Unable to match requested speed 2000 kHz, using 1800 kHz
adapter speed: 1800 kHz
> 

```

### arm-none-eabi-gdb调试程序
```cpp

arm-none-eabi-gdb stm32f4_out.elf
target remote localhost:3333
monitor reset  
monitor halt  
load 

```

```cpp

xz@xiaqiu:~/study/stm32/ubuntu-stm32_3$ arm-none-eabi-gdb stm32f4_out.elf
GNU gdb (7.10-1ubuntu3+9) 7.10
Copyright (C) 2015 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "--host=x86_64-linux-gnu --target=arm-none-eabi".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<http://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
<http://www.gnu.org/software/gdb/documentation/>.
For help, type "help".
Type "apropos word" to search for commands related to "word"...
Reading symbols from stm32f4_out.elf...done.
(gdb) target remote localhost:3333
Remote debugging using localhost:3333
0x08002f80 in ?? ()
(gdb) monitor reset  
Unable to match requested speed 2000 kHz, using 1800 kHz
Unable to match requested speed 2000 kHz, using 1800 kHz
adapter speed: 1800 kHz
(gdb) monitor halt  
target halted due to debug-request, current mode: Handler HardFault
xPSR: 0x40000003 pc: 00000000 msp: 0x464c4550
(gdb) load
Loading section .isr_vector, size 0x188 lma 0x8000000
Loading section .text, size 0x219c lma 0x8000188
Loading section .rodata, size 0x18 lma 0x8002324
Loading section .ARM, size 0x8 lma 0x800233c
Loading section .data, size 0xc lma 0x8002344
Start address 0x80022d8, load size 9040
Transfer rate: 15 KB/sec, 1808 bytes/write.
(gdb) 

```
